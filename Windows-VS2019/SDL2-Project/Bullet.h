#include "SDL2Common.h"
#include "Sprite.h"
#include "Animation.h"
#include "Vector2f.h"

class Animation;
class Vector2f;
class Game;

class Bullet : public Sprite
{
private:

	int state;

	const int SPRITE_HEIGHT = 64;
	const int SPRITE_WIDTH = 32;

	int damage;

	float lifetime;

	float orientation;
	float angleOffset;

public:

	Bullet();
	~Bullet();

	enum BulletState{TRAVEL};

	void init(SDL_Renderer* renderer, Vector2f* position, Vector2f* direction);

	float calculateOrientation(Vector2f* direction);
	
	void update(float deltaTime);
	
	void draw(SDL_Renderer* renderer);

	int getCurrentAnimationState();

	int getDamage();

	bool hasExpired();
};

